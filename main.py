#!/usr/bin/python3
# -*-coding:Utf-8 -*
from time import time
from fonctions import *

alphaChars   = "azertyuiopqsdfghjklmwxcvbn"
numChars        = "0123456789"
specChars       = """&é"'(-è_çà)=+°#{[|`\^@]}*!:;,?./§ù%µ$£<>"""
char_type       = alphaChars.lower() + alphaChars.upper() + numChars + specChars
char_type_size  = 0
charIsMin       = 0
charIsMaj       = 0
charIsNum       = 0
charIsSpec      = 0
choix           = ""
dicoDefaultPath = "dicos/dico_test.txt"
global testCounter

print("""BFSpeed a pour objectif de sensibiliser le grand public à l'importance du choix de mot de pass.
Nous allons tester le temps nécessaire pour cracker un mot de passe sur cet ordinateur. Les caractères acceptés sont:\n""", char_type, "\n")

while choix != "q":

    dicoPath    = ""
    choix       = ""
    charIsMaj   =  0
    charIsMin   =  0
    charIsSpec  =  0
    CharIsNum   =  0
    verbose     = ""
    verboseMode = 0
    while choix.lower() not in ('b', 'q', 'd'):
        choix           = input("""
        Pour une attaque par dictionnaire                : Tapez d
        Pour tester toutes les combinaisons (BruteForce) : Tapez b
        Pour quitter                                     : Tapez q\n\nVotre choix: """)

    if choix.lower() == "q":
        break

    if choix.lower() == "d":
        fileOK = False
        while fileOK is False:
            dicoPath = input("Emplacement du dictionnaire à utiliser (default = {0}): ".format(dicoDefaultPath))
            if dicoPath == "":
                dicoPath = dicoDefaultPath
            try:
                dicoFile = open(dicoPath, "r")
            except FileNotFoundError:
                print ("Erreur d'ouverture du fichier. Il se peut que le chemin spécifié n'existe pas ou que le fichier soit protégé. (", dicoPath,")")
                continue
            fileOK = True

    password        = input("Entrez un mot de passe: ")

    passwordSize    = 0
    char_type       = ""

    # On vérifie quels types de caractères sont contenus dans
    # le pass afin de signifier le nombre max de combinaisons
    for char in password:
        if char in alphaChars.lower():
            if charIsMin == 0:
                print("--> Le mot de passe contient des lettres minuscules")
                charIsMin  = 1
                char_type += alphaChars.lower()
        elif char in alphaChars.upper():
            if charIsMaj == 0:
                print("--> Le mot de passe contient des lettres majuscules")
                charIsMaj = 1
                char_type += alphaChars.upper()
        elif char in numChars:
            if charIsNum == 0:
                print("--> Le mot de passe contient des nombres")
                charIsNum  = 1
                char_type += numChars
        elif char in specChars:
            if charIsSpec == 0:
                print("--> Le mot de passe contient des caractères spéciaux")
                char_type += specChars
                charIsSpec = 1

        passwordSize += 1

    # Recherche du nombre de caractères à tester
    char_type_size = len(char_type)

    # Recherche du nombre de combinaisons possibles
    i=1
    nombreDeCombinaisons = 0
    while i <= passwordSize:
        nombreDeCombinaisons += char_type_size ** i
        i += 1


    # présentation avant le forçage
    print("\nTaille du mot de passe: ", passwordSize, " caractères")
    if choix.lower() == "b":
        print("Nombre de combinaisons possibles: ", nombreDeCombinaisons)
        print("Les caractères suivants seront testés: ", char_type)
    setVerbose()
    print("\n----------------------------------------")
    if choix.lower() == "b":
        print("BRUTEFORCE".center(40))
    elif choix.lower() == "d":
        print("ATTAQUE PAR DICTIONNAIRE".center(40))
    print("----------------------------------------")
    print("- Appuyez sur 'Entrer' pour commencer -")
    print("----------------------------------------")
    input()

    # A titre d'info : le thinkpad T510 teste environ 110 000 combinaisons par secondes en mode verbeux
    # et 1 800 250 combinaisons / secondes en mode non verbeux...
    # Procédure de crack
    if choix.lower() == "b":
        reset()
        startTime = time()
        bruteforce(password, char_type, "", passwordSize)
    elif choix.lower() == "d":
        startTime = time()
        dictionnaire(password, char_type, dicoFile)

    print("----------------------------------------")
    msg = "Temps: " + str(time() - startTime) + " secondes"
    print(msg.center(40))
    print("----------------------------------------")

#sortie du programme
print("\nA bientôt\n")
