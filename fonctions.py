#!/usr/bin/python3
# -*-coding:Utf-8 -*

"""Module fonctions : Comprend quelques fonctions necéssaires pour bfspeed"""

verboseMode = []
testCounter = 10**15
testCounter = 0

def printCracked():
    """printCracked() affiche le message PASSWORD CRACKED! avec une mise en forme sur 40 chars"""
    print(40*"#")
    print("#", "PASSWORD CRACKED!".center(36), "#")
    print(40*"#")

def reset (): #{
    global testCounter
    testCounter = 0
#}

def setVerbose():
    """setVerbose() demande si le programme doit fonctionner en mode verbeux ou non. verboseMode[0] contiendra alors 0(non verbeux) ou 1(verbeux)"""

    verbose = ""
    while verbose.lower() not in ('y', 'n'):
        verbose = input("L'affichage des tests ralenti considérablement le programme. Souhaitez vous l'activer? (Y/N): ")
    verboseMode.clear()
    if verbose.lower() == "y":
        verboseMode.append(1)
    else:
        verboseMode.append(0)

def dictionnaire(password_, char_type_, dicoFile_):
    """Fonction d'attaque par dictionnaire"""

    line="init"
    counter = 0

    while line != "":
        line = dicoFile_.readline()
        line = line.rstrip('\n')
        counter += 1

        # L'affichage du resultat ralenti considérablement l'exécution du script (15 à 16x plus lent!!!)¬
        if verboseMode[0] == 1:
            print(line)
            #input() # Pour debug
        if line == password_:
            printCracked()
            print("Pass = ", line)
            print("Combinaisons testées: ", counter)
            break

def bruteforce(password, char_type, myStr, countFor):
    """Fonction récursive qui permet de tester toutes les combinaisons. Elle prend en argument:
    - le password de l'utilisateur (pour la comparaison
    - une str (char_type) qui contient l'ensemble des caractères à tester
    - Une str (myStr) contenant la string à laquelle ajouter un caractère pour tester (au départ elle vaut "" dans main.py
    - Un int (countFor) qui permet de compter le nombre de récursions"""

    global testCounter
    testCounter += 1

    if countFor == 0:
        return 0

    for x in char_type:

        # L'affichage du resultat ralenti considérablement l'exécution du script (15 à 16x plus lent!!!)
        if verboseMode[0] == 1:
            print(myStr + x)

        if (myStr + x) == password:
            printCracked()
            print("pass : ", myStr + x)
            print(testCounter, " combinaisons testées")
            return 1
        elif (bruteforce(password, char_type, (myStr + x), (countFor - 1))) == 1:
            return 1


