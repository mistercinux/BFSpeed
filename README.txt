BFSpeed doit permettre de présenter de manière simple à un public non informaticien à quelle vitesse un ordinateur peut tester toutes les combinaisons possibles pour forcer un mot de passe.

A faire:
   - Trouver un bon dictionnaire à placer dans le repertoire dicos/

Language :

    J'ai choisi d'utiliser le python afin de faire joujou avec ce langage qui présente l'avantage d'être portable et qui permet de faire aisément ce que j'attends de ce programme


Mode opératoire:

    Cas d'une attaque en testant toutes les combinaisons (a)
        - L'utilisateur entre un mot de passe
        - on effectue un test chronologique des combinaisons:
          a, b, c, ..., aa, ab, ...
        - Quand la combinaison correspond au pass, on enregistre le temps
        - On affiche le résultat sous la forme :
            chaine trouvée = pass 
            XXX Combinaisons testées en XXX minutes

    Cas d'une attaque par dictionnaire:
        - L'utilisateur entre un mot de pass
        - On demande l'emplacement du dictionnaire (définir un emplacement par défaut)
        - On teste les entrées du dictionnaire
        - On affiche le résultat sous la forme :¬
            chaine trouvée = pass⎵¬
            XXX Combinaisons testées en XXX minutes¬

